package com.prodotti.crud.products.controller;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.boot.test.mock.mockito.MockBean;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import java.util.List;

import com.prodotti.crud.products.model.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProductsAMZController.class)
class ProductsAMZControllerTest {

	@MockBean
	ProductsAMZController productsController;
	
	@Test
	public void addNewProduct() {
		productsController = Mockito.mock(ProductsAMZController.class);
		ResponseEntity<PRODOTTI_AMZ> response_AMZ=new ResponseEntity<PRODOTTI_AMZ>(HttpStatus.OK);
		Mockito.when(productsController.createProduct(new PRODOTTI_AMZ("Test","Articolo Test"))).thenReturn(response_AMZ);
	}
	
	@Test
	public void getAllProducts() {
		productsController = Mockito.mock(ProductsAMZController.class);
		ResponseEntity<List<PRODOTTI_AMZ>> response_AMZ=new ResponseEntity<List<PRODOTTI_AMZ>>(HttpStatus.OK);
		String SKU_AMZ = "test";
		productsController.createProduct(new PRODOTTI_AMZ(SKU_AMZ,"Articolo Test"));
		Mockito.when(productsController.getAllProducts_AMZ()).thenReturn(response_AMZ);
	}
	
	@Test
	public void getProductBySKU() {
		productsController = Mockito.mock(ProductsAMZController.class);
		ResponseEntity<PRODOTTI_AMZ> response_AMZ=new ResponseEntity<PRODOTTI_AMZ>(HttpStatus.OK);
		String SKU_AMZ = "test";
		productsController.createProduct(new PRODOTTI_AMZ(SKU_AMZ,"Articolo Test"));
		Mockito.when(productsController.getProdBySKU_AMZ(SKU_AMZ)).thenReturn(response_AMZ);
	}
	
	@Test
	public void updateProduct() {
		productsController = Mockito.mock(ProductsAMZController.class);
		ResponseEntity<PRODOTTI_AMZ> response_AMZ=new ResponseEntity<PRODOTTI_AMZ>(HttpStatus.OK);
		String SKU_AMZ = "test";
		productsController.createProduct(new PRODOTTI_AMZ(SKU_AMZ,"Articolo Test"));
		Mockito.when(productsController.updateProduct(SKU_AMZ, new PRODOTTI_AMZ(SKU_AMZ,"Articolo Test Updatato"))).thenReturn(response_AMZ);
	}
	
	@Test
	public void deleteProduct() {
		productsController = Mockito.mock(ProductsAMZController.class);
		String SKU_AMZ = "test";
		productsController.createProduct(new PRODOTTI_AMZ(SKU_AMZ,"Articolo Test"));
		Mockito.when(productsController.deleteProduct_AMZ(SKU_AMZ)).thenReturn(HttpStatus.OK);
	}

}
