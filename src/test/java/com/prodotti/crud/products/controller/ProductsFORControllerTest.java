package com.prodotti.crud.products.controller;


import java.util.List;


import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.prodotti.crud.products.model.PRODOTTI_FOR;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProductsFORController.class)
class ProductsFORControllerTest {

	@MockBean
	ProductsFORController productsController;
	
	@Test
	public void addNewProduct() {
		productsController = Mockito.mock(ProductsFORController.class);
		ResponseEntity<PRODOTTI_FOR> response_FOR=new ResponseEntity<PRODOTTI_FOR>(HttpStatus.OK);
		Mockito.when(productsController.createProduct(new PRODOTTI_FOR(4L,"Test","Articolo Test"))).thenReturn(response_FOR);
	}
	
	@Test
	public void getAllProducts() {
		productsController = Mockito.mock(ProductsFORController.class);
		ResponseEntity<List<PRODOTTI_FOR>> response_FOR=new ResponseEntity<List<PRODOTTI_FOR>>(HttpStatus.OK);
		String SKU_FOR = "e-128";
		productsController.createProduct(new PRODOTTI_FOR(4L,SKU_FOR,"Articolo Test"));
		Mockito.when(productsController.getAllProducts_FOR()).thenReturn(response_FOR);
	}
	
	@Test
	public void getProductBySKU() {
		productsController = Mockito.mock(ProductsFORController.class);
		ResponseEntity<PRODOTTI_FOR> response_FOR=new ResponseEntity<PRODOTTI_FOR>(HttpStatus.OK);
		String SKU_FOR = "e-128";
		productsController.createProduct(new PRODOTTI_FOR(4L,SKU_FOR,"Articolo Test"));
		Mockito.when(productsController.getProdBySKU_FOR(SKU_FOR)).thenReturn(response_FOR);
	}
	
	@Test
	public void updateProduct() {
		productsController = Mockito.mock(ProductsFORController.class);
		ResponseEntity<PRODOTTI_FOR> response_FOR=new ResponseEntity<PRODOTTI_FOR>(HttpStatus.OK);
		String SKU_FOR = "e-128";
		productsController.createProduct(new PRODOTTI_FOR(4L,SKU_FOR,"Articolo Test"));
		Mockito.when(productsController.updateProduct(SKU_FOR, new PRODOTTI_FOR(4L,SKU_FOR,"Articolo Updatato"))).thenReturn(response_FOR);
	}
	
	@Test
	public void deleteProduct() {
		productsController = Mockito.mock(ProductsFORController.class);
		String SKU_FOR = "e-128";
		productsController.createProduct(new PRODOTTI_FOR(4L,SKU_FOR,"Articolo Test"));
		Mockito.when(productsController.deleteProduct_FOR(SKU_FOR)).thenReturn(HttpStatus.OK);
	}
}
