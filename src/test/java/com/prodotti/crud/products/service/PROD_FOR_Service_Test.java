package com.prodotti.crud.products.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.mock.mockito.MockBean;
import com.prodotti.crud.products.model.PRODOTTI_FOR;

class PROD_FOR_Service_Test {

	@MockBean
	@Autowired
	@Qualifier
	PROD_FOR_Service service_FOR;
	
	@Test
	public void addNewProduct() {
		/*PRODOTTI_FOR prod = new PRODOTTI_FOR(4L,"Test","Articolo Test");
		PRODOTTI_FOR found = new PRODOTTI_FOR(4L,"Test","Articolo Test");
//		PRODOTTI_FOR found = when(service_FOR.getProductBySKU(prod.getSKU())).thenReturn();
		Mockito.doReturn(found).when(service_FOR).getProductBySKU(prod.getSKU());
		assertThat(found.getSKU()).isEqualTo(prod.getSKU());*/
		
		service_FOR = Mockito.mock(PROD_FOR_Service.class);
		PRODOTTI_FOR prod_FOR = new PRODOTTI_FOR(4L,"Test","Articolo Test");
		Mockito.when(service_FOR.createProduct(prod_FOR)).thenReturn(new PRODOTTI_FOR(4L,"Test","Articolo Test"));
	}
	
	@Test
	public void getAllProducts() {
		service_FOR = Mockito.mock(PROD_FOR_Service.class);
		List<PRODOTTI_FOR> list_FOR = new ArrayList<PRODOTTI_FOR>();
		Mockito.when(service_FOR.getAllProducts()).thenReturn(list_FOR);
	}
	
	@Test
	public void getProductBySKU() {
		service_FOR = Mockito.mock(PROD_FOR_Service.class);
		String SKU = "e-128";
		service_FOR.createProduct(new PRODOTTI_FOR(4L,SKU,"Articolo Test"));
		Mockito.when(service_FOR.getProductBySKU(SKU)).thenReturn(new PRODOTTI_FOR(4L,SKU,"Articolo Test"));
	}
	
	@Test
	public void updateProduct() {
		service_FOR = Mockito.mock(PROD_FOR_Service.class);
		String SKU = "e-128";
		service_FOR.createProduct(new PRODOTTI_FOR(4L,SKU,"Articolo Test"));
		PRODOTTI_FOR updated = new PRODOTTI_FOR(4L,SKU,"Articolo updatato");
		Mockito.when(service_FOR.updateProduct(updated, SKU)).thenReturn(new PRODOTTI_FOR(4L,SKU,"Articolo updatato"));
	}
	
	@Test
	public void deleteProduct() {
		service_FOR = Mockito.mock(PROD_FOR_Service.class);
		String SKU = "e-128";
		service_FOR.createProduct(new PRODOTTI_FOR(4L,SKU,"Articolo Test"));
		service_FOR.deleteProduct(SKU);
		Mockito.when(service_FOR.getProductBySKU(SKU)).thenReturn(null);
	}

}
