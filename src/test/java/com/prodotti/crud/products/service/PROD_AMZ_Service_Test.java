package com.prodotti.crud.products.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.prodotti.crud.products.model.PRODOTTI_AMZ;

class PROD_AMZ_Service_Test {

	@MockBean
	@Autowired
	@Qualifier
	PROD_AMZ_Service service_AMZ;
	
	@Test
	public void addNewProduct() {		
		service_AMZ = Mockito.mock(PROD_AMZ_Service.class);
		PRODOTTI_AMZ prod_AMZ = new PRODOTTI_AMZ("Test","Articolo Test");
		Mockito.when(service_AMZ.createProduct(prod_AMZ)).thenReturn(new PRODOTTI_AMZ("Test","Articolo Test"));
	}
	
	@Test
	public void getAllProducts() {
		service_AMZ = Mockito.mock(PROD_AMZ_Service.class);
		List<PRODOTTI_AMZ> list_AMZ = new ArrayList<PRODOTTI_AMZ>();
		Mockito.when(service_AMZ.getAllProducts()).thenReturn(list_AMZ);
	}
	
	@Test
	public void getProductBySKU() {
		service_AMZ = Mockito.mock(PROD_AMZ_Service.class);
		String SKU = "test";
		service_AMZ.createProduct(new PRODOTTI_AMZ(SKU,"Articolo Test"));
		Mockito.when(service_AMZ.getProductBySKU(SKU)).thenReturn(new PRODOTTI_AMZ(SKU,"Articolo Test"));
	}
	
	@Test
	public void updateProduct() {
		service_AMZ = Mockito.mock(PROD_AMZ_Service.class);
		String SKU = "test";
		service_AMZ.createProduct(new PRODOTTI_AMZ(SKU,"Articolo Test"));
		PRODOTTI_AMZ updated_AMZ = new PRODOTTI_AMZ(SKU,"Articolo Test Updatato");
		Mockito.when(service_AMZ.updateProduct(updated_AMZ, SKU)).thenReturn(new PRODOTTI_AMZ(SKU,"Articolo Test Updatato"));
	}
	
	@Test
	public void deleteProduct() {
		service_AMZ = Mockito.mock(PROD_AMZ_Service.class);
		String SKU = "test";
		service_AMZ.createProduct(new PRODOTTI_AMZ(SKU,"Articolo Test"));
		service_AMZ.deleteProduct(SKU);
		Mockito.when(service_AMZ.getProductBySKU(SKU)).thenReturn(null);
	}

}
