package com.prodotti.crud.products;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProductsApplication.class)
class ProductsApplicationTests {

	@Test
	public void contextLoad() {
	}

	@Test
	public void testMainApplicationRun() {
		ProductsApplication.main(new String[] {});
	}

}
