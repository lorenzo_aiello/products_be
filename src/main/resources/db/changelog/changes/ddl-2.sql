CREATE TABLE PRODOTTI_AMZ(
    SKU VARCHAR(20) NOT NULL PRIMARY KEY,
    ARTICOLO VARCHAR(150) NOT NULL,
    CONDIZIONE VARCHAR(30),
    OFFERTA VARCHAR(20),
    QUANTITA INT NOT NULL,
    DATA_ORDINE DATE NOT NULL,
    PREZZO FLOAT NOT NULL,
    TASSE FLOAT,
    SPEDIZIONE VARCHAR(40) NOT NULL,
    TASSA_SPEDIZIONE FLOAT,
    COMMISSIONI FLOAT,
    GUADAGNI FLOAT,
    FOREIGN KEY (SKU) REFERENCES PRODOTTI_FOR(SKU)
);