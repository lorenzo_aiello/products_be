package com.prodotti.crud.products;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.prodotti.crud.products.*")
@EntityScan
@EnableAutoConfiguration
public class ProductsApplication{

	public static void main(String[] args) {
		SpringApplication.run(ProductsApplication.class, args);
	}

}
