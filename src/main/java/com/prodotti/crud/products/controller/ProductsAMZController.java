package com.prodotti.crud.products.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.prodotti.crud.products.model.PRODOTTI_AMZ;
import com.prodotti.crud.products.service.PROD_AMZ_Service;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/products")
@Api(value="Products AMZ Management System", description="Operations pertaining to Products AMZ in Products Management System")
public class ProductsAMZController {
	@Autowired
	@Qualifier
	private PROD_AMZ_Service service_AMZ;
	
	/* C */
	//Create a new Product
	@ApiOperation(value = "Add a Product_AMZ")
	@PostMapping("/products_AMZ")
	@ResponseBody
	public ResponseEntity <PRODOTTI_AMZ> createProduct(@ApiParam(value = "PRODOTTI_AMZ object store in database table", required = true) @Valid @RequestBody PRODOTTI_AMZ newProd) {
		return ResponseEntity.ok().body(this.service_AMZ.createProduct(newProd));
	}
	
	/* R */
	// Get All Products
	@ApiOperation(value = "View a list of available Products_AMZ", response = List.class)
	@ApiResponses(value = {
	    @ApiResponse(code = 200, message = "Successfully retrieved list"),
	    @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	    @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	    @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
	})
	@GetMapping("/products_AMZ")
	@ResponseBody
	public ResponseEntity<List<PRODOTTI_AMZ>> getAllProducts_AMZ(){
		return ResponseEntity.ok().body(service_AMZ.getAllProducts());
	}
	
	// Get a Single Product
	@ApiOperation(value = "Get a Product_AMZ by SKU")
	@GetMapping("/products_AMZ/{SKU}")
	@ResponseBody
	public ResponseEntity <PRODOTTI_AMZ> getProdBySKU_AMZ(@ApiParam(value = "Product SKU from which product object will retrieve", required = true) @PathVariable(value = "SKU") String SKU) {
		return ResponseEntity.ok().body(service_AMZ.getProductBySKU(SKU));
	}
	
	/* U */
	// Update a Product
	@ApiOperation(value = "Update a Product_AMZ")
	@PutMapping("/products_AMZ/{SKU}")
	@ResponseBody
	public ResponseEntity <PRODOTTI_AMZ> updateProduct(@ApiParam(value = "Product SKU to update product object", required = true) @PathVariable(value = "SKU") String SKU, @ApiParam(value = "Update product object", required = true) @Valid @RequestBody PRODOTTI_AMZ prodDetails) {
		prodDetails.setSKU(SKU);
        return ResponseEntity.ok().body(this.service_AMZ.updateProduct(prodDetails,SKU));
	}
	
	/* D */
	// Delete a Product
	@ApiOperation(value = "Delete a Product_AMZ")
	@DeleteMapping("/products_AMZ/{SKU}")
	@ResponseBody
	public HttpStatus deleteProduct_AMZ(@ApiParam(value = "Product SKU from which product object will delete from database table", required = true) @PathVariable(value = "SKU") String SKU) {
		this.service_AMZ.deleteProduct(SKU);
        return HttpStatus.OK;
	}

}
