package com.prodotti.crud.products.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.prodotti.crud.products.model.PRODOTTI_FOR;
import com.prodotti.crud.products.service.PROD_FOR_Service;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/products")
@Api(value="Products FOR Management System", description="Operations pertaining to Products FOR in Products Management System")
public class ProductsFORController {
		
	@Autowired
	@Qualifier
	private PROD_FOR_Service service_FOR;
	
	/* C */
	//Create a new Product
	@ApiOperation(value = "Add a Product_FOR")
	@PostMapping("/products_FOR")
	@ResponseBody
	public ResponseEntity <PRODOTTI_FOR> createProduct(@ApiParam(value = "PRODOTTI_FOR object store in database table", required = true) @Valid @RequestBody PRODOTTI_FOR newProd) {
		return ResponseEntity.ok().body(this.service_FOR.createProduct(newProd));
	}
	
	/* R */
	// Get All Products
	@ApiOperation(value = "View a list of available Products_FOR", response = List.class)
	@ApiResponses(value = {
	    @ApiResponse(code = 200, message = "Successfully retrieved list"),
	    @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	    @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	    @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
	})
	@GetMapping("/products_FOR")
	@ResponseBody
	public ResponseEntity<List<PRODOTTI_FOR>> getAllProducts_FOR() {
		return ResponseEntity.ok().body(service_FOR.getAllProducts());
	}
	
	// Get a Single Product
	@ApiOperation(value = "Get a Product_FOR by SKU")
	@GetMapping("/products_FOR/{SKU}")
	@ResponseBody
	public ResponseEntity <PRODOTTI_FOR> getProdBySKU_FOR(@ApiParam(value = "Product SKU from which product object will retrieve", required = true) @PathVariable(value = "SKU") String SKU) {
		return ResponseEntity.ok().body(service_FOR.getProductBySKU(SKU));
	}
	
	/* U */
	// Update a Product
	@ApiOperation(value = "Update a Product_FOR")
	@PutMapping("/products_FOR/{SKU}")
	@ResponseBody
	public ResponseEntity <PRODOTTI_FOR> updateProduct(@ApiParam(value = "Product SKU to update product object", required = true) @PathVariable(value = "SKU") String SKU, @ApiParam(value = "Update product object", required = true) @Valid @RequestBody PRODOTTI_FOR prodDetails) {
		prodDetails.setSKU(SKU);
        return ResponseEntity.ok().body(this.service_FOR.updateProduct(prodDetails,SKU));
	}
	
	/* D */
	// Delete a Product
	@ApiOperation(value = "Delete a Product_FOR")
	@DeleteMapping("/products_FOR/{SKU}")
	@ResponseBody
	public HttpStatus deleteProduct_FOR(@ApiParam(value = "Product SKU from which product object will delete from database table", required = true) @PathVariable(value = "SKU") String SKU) {
		this.service_FOR.deleteProduct(SKU);
        return HttpStatus.OK;
	}

}
