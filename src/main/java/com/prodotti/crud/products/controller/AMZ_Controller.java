package com.prodotti.crud.products.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.prodotti.crud.products.model.PRODOTTI_AMZ;
import com.prodotti.crud.products.service.PROD_AMZ_Service;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/")
@RestController
@Api(value="Products AMZ Management System", description="Operations pertaining to Products AMZ in Products Management System")
public class AMZ_Controller extends ProductsAMZController{
	@Autowired
	@Qualifier
	private PROD_AMZ_Service service_AMZ;
	
	// Get All Products
		@Override
		@ApiOperation(value = "View a list of available Products_AMZ", response = List.class)
		@ApiResponses(value = {
		    @ApiResponse(code = 200, message = "Successfully retrieved list"),
		    @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
		    @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
		    @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
		})
		@GetMapping("/products_AMZ")
		@ResponseBody
		public ResponseEntity<List<PRODOTTI_AMZ>> getAllProducts_AMZ(){
			return ResponseEntity.ok().body(service_AMZ.getAllProducts());
		}
		
		@ApiOperation(value = "Get a Product_AMZ by ARTICOLO")
		@GetMapping("/products_AMZ/{ARTICOLO}")
		@ResponseBody
		public ResponseEntity <PRODOTTI_AMZ> getProdByArticoloAMZ(@ApiParam(value = "Product ARTICOLO from which product object will retrieve", required = true) @PathVariable(value = "ARTICOLO") String ARTICOLO) {
			return ResponseEntity.ok().body(service_AMZ.getProductByARTICOLO(ARTICOLO));
		}
}
