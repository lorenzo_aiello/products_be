package com.prodotti.crud.products.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.prodotti.crud.products.model.PRODOTTI_FOR;

@Repository
public interface PROD_FOR_REPOSITORY extends JpaRepository<PRODOTTI_FOR, String> {

	List<PRODOTTI_FOR> findBySKU(@Param("SKU")String SKU); //query that search for SKU in a list of Strings
	
	@Query("SELECT MAX(SEQ_PROD) FROM PRODOTTI_FOR")
	Long getMaxSeqProd();
		
}