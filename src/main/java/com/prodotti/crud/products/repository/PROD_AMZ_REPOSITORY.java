package com.prodotti.crud.products.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.prodotti.crud.products.model.PRODOTTI_AMZ;

@Repository
public interface PROD_AMZ_REPOSITORY extends JpaRepository<PRODOTTI_AMZ, String> {
	List<PRODOTTI_AMZ> findBySKU(@Param("SKU")String SKU); //query that search for SKU in a list of Strings
	PRODOTTI_AMZ findByARTICOLO(@Param("ARTICOLO")String ARTICOLO);
}
