package com.prodotti.crud.products.service;

import java.util.List;

import com.prodotti.crud.products.model.PRODOTTI_FOR;

public interface PROD_FOR_Service{
	public PRODOTTI_FOR createProduct(PRODOTTI_FOR product);

	public PRODOTTI_FOR updateProduct(PRODOTTI_FOR product, String SKU);

	public List<PRODOTTI_FOR> getAllProducts();

	public PRODOTTI_FOR getProductBySKU(String SKU);

	public void deleteProduct(String SKU);
}
