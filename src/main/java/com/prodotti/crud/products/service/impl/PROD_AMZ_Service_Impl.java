package com.prodotti.crud.products.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.prodotti.crud.products.model.PRODOTTI_AMZ;
import com.prodotti.crud.products.repository.PROD_AMZ_REPOSITORY;
import com.prodotti.crud.products.service.PROD_AMZ_Service;
import com.prodotti.crud.products.exception.*;

@Service
@Qualifier
public class PROD_AMZ_Service_Impl implements PROD_AMZ_Service{
	
	@Autowired
	PROD_AMZ_REPOSITORY prod_for_amz;
	@Autowired
	com.prodotti.crud.products.repository.PROD_FOR_REPOSITORY prod_FOR;

	@Override
	public PRODOTTI_AMZ createProduct(PRODOTTI_AMZ product) {
		String articolo = prod_FOR.findBySKU(product.getSKU()).get(0).getARTICOLO();
		product.setARTICOLO(articolo);
		return prod_for_amz.save(product);
	}

	@Override
	public PRODOTTI_AMZ updateProduct(PRODOTTI_AMZ product, String SKU) throws ResourceNotFoundException {

		PRODOTTI_AMZ prod = prod_for_amz.findById(SKU)
				.orElseThrow(() -> new ResourceNotFoundException("Product_AMZ not found for this SKU :: " + SKU));
		prod.setSKU(product.getSKU());
		prod.setARTICOLO(product.getARTICOLO());
		prod.setCONDIZIONE(product.getCONDIZIONE());
		prod.setOFFERTA(product.getOFFERTA());
		prod.setQUANTITA(product.getQUANTITA());
		prod.setDATA_ORDINE(product.getDATA_ORDINE());
		prod.setPREZZO(product.getPREZZO());
		prod.setTASSE(product.getTASSE());
		prod.setSPEDIZIONE(product.getSPEDIZIONE());
		prod.setTASSA_SPEDIZIONE(product.getTASSA_SPEDIZIONE());
		prod.setCOMMISSIONI(product.getCOMMISSIONI());
		prod.setGUADAGNI(product.getGUADAGNI());
		PRODOTTI_AMZ updatedProduct = prod_for_amz.save(prod);
	    return updatedProduct;
	}

	@Override
	public List<PRODOTTI_AMZ> getAllProducts() {
		return this.prod_for_amz.findAll();
	}

	@Override
	public PRODOTTI_AMZ getProductBySKU(String SKU) throws ResourceNotFoundException {
		PRODOTTI_AMZ prod =  prod_for_amz.findById(SKU)
				.orElseThrow(() -> new ResourceNotFoundException("Product_AMZ not found for this SKU :: " + SKU));
		return prod;
	}

	@Override
	public void deleteProduct(String SKU) throws ResourceNotFoundException {
		PRODOTTI_AMZ prodToDelete = prod_for_amz.findById(SKU)
				.orElseThrow(() -> new ResourceNotFoundException("Product_AMZ not found for this SKU :: " + SKU));
		prod_for_amz.delete(prodToDelete);
		
	}

	@Override
	public PRODOTTI_AMZ getProductByARTICOLO(String ARTICOLO) {
		PRODOTTI_AMZ prod =  prod_for_amz.findByARTICOLO(ARTICOLO);
		return prod;
	}

}
