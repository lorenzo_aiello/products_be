package com.prodotti.crud.products.service;

import java.util.List;

import com.prodotti.crud.products.model.PRODOTTI_AMZ;

public interface PROD_AMZ_Service{
	public PRODOTTI_AMZ createProduct(PRODOTTI_AMZ product);

	public PRODOTTI_AMZ updateProduct(PRODOTTI_AMZ product, String SKU);

	public List<PRODOTTI_AMZ> getAllProducts();

	public PRODOTTI_AMZ getProductBySKU(String SKU);
	
	public PRODOTTI_AMZ getProductByARTICOLO(String ARTICOLO);

	public void deleteProduct(String SKU);
}
