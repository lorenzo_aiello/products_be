package com.prodotti.crud.products.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.prodotti.crud.products.model.PRODOTTI_FOR;
import com.prodotti.crud.products.repository.PROD_FOR_REPOSITORY;
import com.prodotti.crud.products.service.PROD_FOR_Service;
import com.prodotti.crud.products.exception.*;

@Service
@Qualifier
public class PROD_FOR_Service_Impl implements PROD_FOR_Service{
	
	@Autowired
	PROD_FOR_REPOSITORY prod_for_repo;

	@Override
	public PRODOTTI_FOR createProduct(PRODOTTI_FOR product) {
		Long seq = prod_for_repo.getMaxSeqProd();
		if(seq!=null)
			product.setSEQ_PROD(seq+1);
		else
			product.setSEQ_PROD(1L);
		return prod_for_repo.save(product);
	}

	@Override
	public PRODOTTI_FOR updateProduct(PRODOTTI_FOR product, String SKU) throws ResourceNotFoundException {
		PRODOTTI_FOR prod = prod_for_repo.findById(SKU)
			        .orElseThrow(() -> new ResourceNotFoundException("Product_FOR not found for this SKU :: " + SKU));
		prod.setSKU(product.getSKU());
		prod.setARTICOLO(product.getARTICOLO());
		prod.setIMPONIBILE(product.getIMPONIBILE());
		prod.setIVA(product.getIVA());
		prod.setPREZZO(product.getPREZZO());
		prod.setPESO(product.getPESO());
		prod.setPREZZO_SPEDIZIONE(product.getPREZZO_SPEDIZIONE());
		prod.setDATA_INIZIO_VAL(product.getDATA_INIZIO_VAL());
		prod.setDATA_FINE_VAL(product.getDATA_FINE_VAL());
		PRODOTTI_FOR updatedProduct = prod_for_repo.save(prod);
	    return updatedProduct;
	}

	@Override
	public List<PRODOTTI_FOR> getAllProducts() {
		return this.prod_for_repo.findAll();
	}

	@Override
	public PRODOTTI_FOR getProductBySKU(String SKU) throws ResourceNotFoundException {
		PRODOTTI_FOR prod = prod_for_repo.findById(SKU)
		          .orElseThrow(() -> new ResourceNotFoundException("Product_FOR not found for this SKU :: " + SKU));
		return prod;
	}

	@Override
	public void deleteProduct(String SKU) throws ResourceNotFoundException {
		PRODOTTI_FOR prodToDelete = prod_for_repo.findById(SKU)
			       .orElseThrow(() -> new ResourceNotFoundException("Product_FOR not found for this SKU :: " + SKU));
		prod_for_repo.delete(prodToDelete);	
	}

}
