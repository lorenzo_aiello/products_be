package com.prodotti.crud.products.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@ApiModel(value="PRODOTTI_AMZ", description = "All details about the PRODOTTI_AMZ. ")
public class PRODOTTI_AMZ{
	
	@ApiModelProperty(notes = "The database generated PRODOTTI_AMZ SKU")
    @Id
	private String SKU;
	
	@ApiModelProperty(notes = "The Product articolo")
	private String ARTICOLO;
	
	@ApiModelProperty(notes = "The Product quantità")
	private String CONDIZIONE;
	
	@ApiModelProperty(notes = "The Product quantità")
	private String OFFERTA;
	
	@ApiModelProperty(notes = "The Product quantità")
	private int QUANTITA;
	
	@ApiModelProperty(notes = "The Product data ordine")
	@Column(name = "DATA_ORDINE")
	private Date DATA_ORDINE;
	
	@ApiModelProperty(notes = "The Product prezzo")
	private float PREZZO;
	
	@ApiModelProperty(notes = "The Product tasse")
	private float TASSE;
	
	@ApiModelProperty(notes = "The Product spedizione")
	private String SPEDIZIONE;
	
	@ApiModelProperty(notes = "The Product tassa_spedizione")
	private float TASSA_SPEDIZIONE;
	
	@ApiModelProperty(notes = "The Product commissioni")
	private float COMMISSIONI;
	
	@ApiModelProperty(notes = "The Product guadagni")
	private float GUADAGNI;

	protected PRODOTTI_AMZ() {}
	
	public PRODOTTI_AMZ(String sKU, String aRTICOLO, String cONDIZIONE, String oFFERTA, int qUANTITA, Date dATA_ORDINE,
			float pREZZO, float tASSE, String sPEDIZIONE, float tASSA_SPEDIZIONE, float cOMMISSIONI, float gUADAGNI) {
		super();
		SKU = sKU;
		ARTICOLO = aRTICOLO;
		CONDIZIONE = cONDIZIONE;
		OFFERTA = oFFERTA;
		QUANTITA = qUANTITA;
		DATA_ORDINE = dATA_ORDINE;
		PREZZO = pREZZO;
		TASSE = tASSE;
		SPEDIZIONE = sPEDIZIONE;
		TASSA_SPEDIZIONE = tASSA_SPEDIZIONE;
		COMMISSIONI = cOMMISSIONI;
		GUADAGNI = gUADAGNI;
	}
	
	public PRODOTTI_AMZ(String SKU, String ARTICOLO) {
		this.SKU = SKU;
		this.ARTICOLO = ARTICOLO;
	}

	public String getSKU() {
		return SKU;
	}

	public void setSKU(String sKU) {
		SKU = sKU;
	}

	public String getARTICOLO() {
		return ARTICOLO;
	}

	public void setARTICOLO(String aRTICOLO) {
		ARTICOLO = aRTICOLO;
	}

	public String getCONDIZIONE() {
		return CONDIZIONE;
	}

	public void setCONDIZIONE(String cONDIZIONE) {
		CONDIZIONE = cONDIZIONE;
	}

	public String getOFFERTA() {
		return OFFERTA;
	}

	public void setOFFERTA(String oFFERTA) {
		OFFERTA = oFFERTA;
	}

	public int getQUANTITA() {
		return QUANTITA;
	}

	public void setQUANTITA(int qUANTITA) {
		QUANTITA = qUANTITA;
	}

	public Date getDATA_ORDINE() {
		return DATA_ORDINE;
	}

	public void setDATA_ORDINE(Date dATA_ORDINE) {
		DATA_ORDINE = dATA_ORDINE;
	}

	public float getPREZZO() {
		return PREZZO;
	}

	public void setPREZZO(float pREZZO) {
		PREZZO = pREZZO;
	}

	public float getTASSE() {
		return TASSE;
	}

	public void setTASSE(float tASSE) {
		TASSE = tASSE;
	}

	public String getSPEDIZIONE() {
		return SPEDIZIONE;
	}

	public void setSPEDIZIONE(String sPEDIZIONE) {
		SPEDIZIONE = sPEDIZIONE;
	}

	public float getTASSA_SPEDIZIONE() {
		return TASSA_SPEDIZIONE;
	}

	public void setTASSA_SPEDIZIONE(float tASSA_SPEDIZIONE) {
		TASSA_SPEDIZIONE = tASSA_SPEDIZIONE;
	}

	public float getCOMMISSIONI() {
		return COMMISSIONI;
	}

	public void setCOMMISSIONI(float cOMMISSIONI) {
		COMMISSIONI = cOMMISSIONI;
	}

	public float getGUADAGNI() {
		return GUADAGNI;
	}

	public void setGUADAGNI(float gUADAGNI) {
		GUADAGNI = gUADAGNI;
	}

}
