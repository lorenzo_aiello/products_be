package com.prodotti.crud.products.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
import javax.persistence.Id;
//import javax.persistence.SequenceGenerator;
import javax.persistence.SequenceGenerator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@ApiModel(value="PRODOTTI_FOR", description = "All details about the PRODOTTI_FOR. ")
public class PRODOTTI_FOR{
	
	@ApiModelProperty(notes = "The Product seq_prod")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PROD")
    //@SequenceGenerator(sequenceName = "SEQ_PROD", name = "SEQ_PROD", allocationSize = 1, initialValue = 1)
    @SequenceGenerator(sequenceName = "SEQ_PROD", allocationSize = 1, name = "SEQ_PROD")
    private Long SEQ_PROD;
	
	@ApiModelProperty(notes = "The database generated PRODOTTI_FOR SKU")
    @Id
	private String SKU;
	
	@ApiModelProperty(notes = "The Product articolo")
	private String ARTICOLO;
	
	@ApiModelProperty(notes = "The Product imponibile")
	private float IMPONIBILE;
	
	@ApiModelProperty(notes = "The Product IVA")
	private float IVA;
	
	@ApiModelProperty(notes = "The Product prezzo")
	private float PREZZO;
	
	@ApiModelProperty(notes = "The Product peso")
	private float PESO;
	
	@ApiModelProperty(notes = "The Product prezzo_spedizione")
	private float PREZZO_SPEDIZIONE;
	
	@ApiModelProperty(notes = "The Product data_inizio_val")
	@Column(name = "DATA_INIZIO_VAL")
	private Date DATA_INIZIO_VAL;
	
	@ApiModelProperty(notes = "The Product data_fine_val")
	@Column(name = "DATA_FINE_VAL")
	private Date DATA_FINE_VAL;
	
	public PRODOTTI_FOR() {}
	
	public PRODOTTI_FOR(Long SEQ_PROD, String SKU, String articolo, float imponibile, float iva, float prezzo, float peso, float prezzo_spedizione, Date dataIn, Date dataFin) {
		this.SEQ_PROD = SEQ_PROD;
		this.SKU = SKU;
		this.ARTICOLO = articolo;
		this.IMPONIBILE = imponibile;
		this.IVA = iva;
		this.PREZZO = prezzo;
		this.PESO = peso;
		this.PREZZO_SPEDIZIONE = prezzo_spedizione;
		this.DATA_INIZIO_VAL = dataIn;
		this.DATA_FINE_VAL = dataFin;
	}
	
	public PRODOTTI_FOR(Long SEQ_PROD, String SKU, String ARTICOLO) {
		this.SEQ_PROD = SEQ_PROD;
		this.SKU = SKU;
		this.ARTICOLO = ARTICOLO;
	}
	
	public void setSEQ_PROD(Long SEQ_PROD) {
		this.SEQ_PROD = SEQ_PROD;
	}
	
	public void setSKU(String SKU) {
		this.SKU = SKU;
	}
	
	public void setARTICOLO(String ARTICOLO) {
		this.ARTICOLO = ARTICOLO;
	}
	
	public void setIMPONIBILE(float IMPONIBILE) {
		this.IMPONIBILE = IMPONIBILE;
	}
	
	public void setIVA(float IVA) {
		this.IVA = IVA;
	}
	
	public void setPREZZO(float PREZZO) {
		this.PREZZO = PREZZO;
	}
	
	public void setPESO(float PESO) {
		this.PESO = PESO;
	}
	
	public void setPREZZO_SPEDIZIONE(float PREZZO_SPEDIZIONE) {
		this.PREZZO_SPEDIZIONE = PREZZO_SPEDIZIONE;
	}
	
	public void setDATA_INIZIO_VAL(Date DATA_INIZIO_VAL) {
		this.DATA_INIZIO_VAL = DATA_INIZIO_VAL;
	}
	
	public void setDATA_FINE_VAL(Date DATA_FINE_VAL) {
		this.DATA_FINE_VAL = DATA_FINE_VAL;
	}
	
	public Long getSEQ_PROD() {
		return SEQ_PROD;
	}
	
	public String getSKU() {
		return SKU;
	}
	
	public String getARTICOLO() {
		return ARTICOLO;
	}
	
	public float getIMPONIBILE() {
		return IMPONIBILE;
	}
	
	public float getIVA() {
		return IVA;
	}
	
	public float getPREZZO() {
		return PREZZO;
	}
	
	public float getPESO() {
		return PESO;
	}
	
	public float getPREZZO_SPEDIZIONE() {
		return PREZZO_SPEDIZIONE;
	}
	
	public Date getDATA_INIZIO_VAL() {
		return DATA_INIZIO_VAL;
	}
	
	public Date getDATA_FINE_VAL() {
		return DATA_FINE_VAL;
	}
	
	@Override
    public String toString() {
        return "PRODOTTI_FOR [SEQ_PROD=" + SEQ_PROD + ", SKU=" + SKU + ", Articolo=" + ARTICOLO + "]";
    }
}
